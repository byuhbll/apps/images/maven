FROM maven:3-ibm-semeru-17-focal

COPY settings.xml /root/.m2/settings.xml

RUN apt update && \
  apt install -y gnupg && \
  apt clean

