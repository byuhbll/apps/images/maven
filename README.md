# Maven Image for Building, Testing, and Deploying

Base Image for building, testing, and deploying primarily in GitLab pipelines.

### System Properties ###

The following system properties are supported for configuring this image to publish to a custom maven repository or to 
maven central. In addition to these system properties, you must also provide the encrypted master password in 
`/root/.m2/settings-security.xml`, the PGP key used for signing the artifact if publishing to maven central, and the 
ssh key used to connect to your internal maven repo if publishing there.

##### SONATYPE_USERNAME #####

The username of the sonatype user for publishing to maven central.

##### ENCRYPTED_SONATYPE_PASSWORD #####

The encrypted sonatype password for publishing to maven central.

##### ENCRYPTED_PGP_PASSPHRASE #####

The encrypted passphrase for accessing the pgp key

##### INTERNAL_MAVEN_REPO_URL #####

The url to the internal maven repo to publish to.

